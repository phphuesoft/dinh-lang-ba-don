<?php get_header(); ?>
<section class="container hs-section-top">
    <div class="hs-article-detail">
        <?php if ( have_posts() ) : the_post(); ?>

            <h1 class="hs-title hs-title-normal text-uppercase text-center"><span class="title-span"><?php the_title(); ?></span></h1>
            <?php the_content(); ?>
                <?php
                $hs_album = Huesoft_get_field('hs_gallery',get_the_ID());
                if($hs_album){
                    ?>
                    <div class="row">
                    <?php
                    foreach ($hs_album as $hs_gallery):
                        ?>
                        <div class="col-md-4 col-sm-6 hs-media-list">
                            <div class="library-item">
                                <div class="box-image hs-item-thumbnail">
                                    <a href="<?php echo $hs_gallery['url']; ?>" class="fancybox-home" data-fancybox-group="button" title="<?php echo $hs_gallery['caption'];?>">
                                        <img src="<?php echo $hs_gallery['url']; ?>" alt="<?php echo $hs_gallery['caption'];?>" width="100%">
                                        <i class="fa fa-search-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach;
                    ?>
                    </div>
                    <?php
                }
                ?>
            <?php
            $hs_video = Huesoft_get_field('hs_iframe_video',get_the_ID());
            if($hs_video):
                ?>
                <div class="embed-responsive embed-responsive-16by9 hs-des-width">
                    <?php echo $hs_video;?>
                </div>
            <?php
            endif;
            ?>

        <?php endif; ?>
    </div>
    <?php Huesoft_related_post(); ?>
</section>
<?php get_footer(); ?>