<?php
/*
 * Template Name: Contact us
 * @package Huesoft
 */
?>
<?php global $options;?>
<?php get_header(); ?>
<section class="container hs-section-top">
<div class="hs-article-detail">
    <?php if ( have_posts() ) : the_post(); ?>
        <h1 class="hs-title hs-title-normal text-uppercase text-center"><span class="title-span"><?php the_title();?></span></h1>
        <div class="row">
            <div class="col-lg-6">
                <h2 class="hs-title-contact"><?php _e('Đình Làng Ba Đồn Quảng Bình','Huesoft');?></h2>
                <div class="hs-content-contact">
                    <p><i class="fa fa-phone"></i> <a href="tel:<?php huesoft_the_field('hs_phone_contact')?>"><?php huesoft_the_field('hs_phone_contact')?></a></p>
                    <p><i class="fa fa-fax"></i> <?php huesoft_the_field('hs_fax_contact')?></p>
                    <p><i class="fa fa-envelope"></i> <a href="mailto:<?php huesoft_the_field('hs_email_contact')?>"><?php huesoft_the_field('hs_email_contact')?></a></p>
                    <p><i class="fa fa-map-marker"></i> <?php huesoft_the_field('hs_address_contact')?></p>
                    <?php the_content();?>
                </div>
            </div>
            <div class="col-lg-6">
                <p class="hs-title-request text-center text-uppercase"><span><?php _e('Gửi Yêu Cầu Cho Chúng Tôi','Huesoft');?></span></p>
                <div class="frmContact">
                    <?php echo do_shortcode(CODE_CONTACT);?>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
</section>
<?php get_footer(); ?>
