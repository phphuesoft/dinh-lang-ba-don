<?php global $options;?>
<footer class="hs-footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6 hs-config">
                <div class="row">
                    <div class="col-md-5 col-lg-4">
                        <a class="hs-logo-footer" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <img src="<?php Huesoft_the_image_base64('/lib/images/logo.png') ?>" alt="Logo Đình Làng Ba Đồn Quảng Bình"/>
                        </a>
                    </div>
                    <div class="col-md-7 col-lg-8 d-flex align-items-center">
                        <div class="hs-info">
                            <?php
                            $loop_contact = new WP_Query('page_id='. ID_PAGE_CONTACT);
                            if ($loop_contact->have_posts()): $loop_contact->the_post(); ?>
                                <h5 class="text-uppercase"><?php _e('Đình Làng Ba Đồn Quảng Bình','Huesoft');?></h5>
                                <p><strong><?php _e('Địa chỉ','Huesoft');?>:</strong> <?php huesoft_the_field('hs_address_contact')?></p>
                                <p><strong><?php _e('Điện thoại','Huesoft');?>:</strong> <?php huesoft_the_field('hs_phone_contact')?></p>
                                <p><strong><?php _e('Fax','Huesoft');?>:</strong> <?php huesoft_the_field('hs_fax_contact')?></p>
                                <p><strong><?php _e('Email','Huesoft');?>:</strong> <?php huesoft_the_field('hs_email_contact')?></p>
                                <?php if (!empty(Huesoft_get_field('hs_date_contact'))){ ?>
                                    <p><strong><?php _e('Mã số thuế','Huesoft');?>:</strong> <?php huesoft_the_field('hs_business_contact')?></p>
                                    <p><strong><?php _e('Ngày cấp','Huesoft');?>:</strong> <?php huesoft_the_field('hs_date_contact')?></p>
                                    <p><strong><?php _e('Nơi cấp','Huesoft');?>:</strong> <?php huesoft_the_field('hs_place_contact')?></p>
                                <?php }?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-5 col-lg-4">
                <div class="hs-policy">
                    <h5 class="hs-title-bot text-uppercase"><?php _e('Điều Khoản Và Chính Sách','Huesoft');?></h5>
                    <?php
                    wp_nav_menu(
                        array(
                            'theme_location'  => 'menu_policy',
                            'menu_class'      => 'terms-list',
                        )
                    );
                    ?>
                    <?php if($options['trade']){?>
                    <div class="hs-trade">
                        <a target="_blank" href="<?php echo $options['trade'] ?>">
                            <img class="bct" src="<?=bloginfo('template_url')?>/lib/images/bocongthuong.png" alt="dang ky bo cong thuong" />
                        </a>
                    </div>
                    <?php }?>
                </div>
            </div>
            <div class="col-sm-6 col-md-7 col-lg-2">
                <?php
                wp_nav_menu(
                    array(
                        'theme_location'  => 'menu_bottom',
                        'menu_class'      => 'terms-list menu-list',
                    )
                );
                ?>
                <div class="hs-follow">
                    <ul class="list-unstyled">
                        <li><a href="<?php echo $options['facebook'];?>" title="facebook"><img src="<?php Huesoft_the_image_base64('/lib/images/facebook.png')?>" alt="facebook"></a></li>
                        <li><a href="<?php echo $options['google'];?>" title="google"><img src="<?php Huesoft_the_image_base64('/lib/images/google.png')?>" alt="google"></a></li>
                        <li><a href="<?php echo $options['twitter'];?>" title="twitter"><img src="<?php Huesoft_the_image_base64('/lib/images/twitter.png')?>" alt="twitter"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<footer style="background-color: #b08932;">
    <div class="container">
        <div class="hs-reserved d-flex justify-content-between">
            <p class="hs-design">
                <?php _e('Thiết kế bởi','Huesoft');?> <a href="http://huesoft.com.vn/" target="_blank">Huesoft</a>
            </p>
            <p class="hs-allright">
                &copy; <?php _e('Đình Làng Ba Đồn 2018','Huesoft');?>
            </p>
        </div>
    </div>
</footer>
<!-- Modal -->
<div id="showModalYoutube"></div>
<?php wp_footer(); ?>
<script>
    (function($){
        $(".fancybox-home").fancybox({
            padding: 0,
            openEffect: 'none',
            closeEffect: 'none',

            prevEffect: 'none',
            nextEffect: 'none',

            closeBtn: false,

            helpers: {
                title: {
                    type: 'outside'
                },
                buttons: {}
            }
        });
    })(jQuery);
</script>
<script>
    $ = jQuery.noConflict();
    function checkBox() {
        s = $.trim($('#txtAnswerQA').val());
        if (s == "") {
            alert("<?php _e('Vui lòng nhập câu hỏi!','Huesoft');?>");
            $('#txtAnswerQA').focus();
            return false;
        }
        s = $.trim($('#txtNameQA').val());
        if (s == "") {
            alert("<?php _e('Vui lòng nhập họ tên!','Huesoft');?>");
            $('#txtNameQA').focus();
            return false;
        }
        email = $.trim($('#txtEmailQA').val());
        if (email == "") {
            alert("<?php _e('Vui lòng nhập địa chỉ Email!','Huesoft');?>");
            $('#txtEmailQA').focus();
            return false;
        }
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            alert("<?php _e('Địa chỉ email không đúng!','Huesoft');?>");
            $("#txtEmailQA").focus();
            return false;
        }
        s = $.trim($('#g-recaptcha-response').val());
        if (s == "") {
            alert("<?php _e('Vui lòng click vào mã bảo vệ!','Huesoft');?>");
            return false;
        }
        return true;
    }
</script>
<?php if (is_home()):?>
<script>
    $ = jQuery.noConflict();
    function showModal(id){
        $('.play'+id).find('.fa').removeClass('fa-play').addClass('fa-spinner fa-spin');
        $.ajax({
            type : "get", //Phương thức truyền post hoặc get
            dataType : "json", //Dạng dữ liệu trả về xml, json, script, or html
            url : '<?php echo admin_url('admin-ajax.php');?>', //Đường dẫn chứa hàm xử lý dữ liệu. Mặc định của WP như vậy
            data : {
                action: "modal", //Tên action
                post_id: id
            },
            beforeSend: function(){

            },
            success: function(response) {
                //Làm gì đó khi dữ liệu đã được xử lý
                if(response.success) {
                    $('#showModalYoutube').html(response.data);
                    $('#showModal').modal('show');
                }
                else {
                    alert('Đã có lỗi xảy ra');
                }
                $('.home').show();
                $('.play'+id).find('.fa').addClass('fa-play').removeClass('fa-spinner fa-spin');
            },
            error: function( jqXHR, textStatus, errorThrown ){
                //Làm gì đó khi có lỗi xảy ra
                console.log( 'The following error occured: ' + textStatus, errorThrown );
            }
        })
    }
</script>
<?php endif;?>
</body>
</html>
