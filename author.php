<?php get_header(); ?>

	<?php if ( have_posts() ) : while( have_posts() ) : the_post(); ?>
		<a href="<?php the_permalink(); ?>"></a>
        <?php the_title();?>  
        <?php Huesoft_entry_content(); ?>  
	<?php endwhile ?>
	<?php wp_pagination(); ?>
	<?php endif; ?>

	<?php get_sidebar(); ?>

<?php get_footer(); ?>