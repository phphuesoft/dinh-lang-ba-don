$ = jQuery.noConflict();
function frmQA() {
    if (checkBox()) {
        $.ajax({
            type: 'POST',
            url: url_ajax.ajaxurl,
            data: {
                // wp ajax action
                action: 'ajax-questionSubmit',
                // vars
                txtNameQA: $('#txtNameQA').val(),
                txtEmailQA: $('#txtEmailQA').val(),
                txtAnswerQA: $('#txtAnswerQA').val(),
                greCaptchaResponse: $('#g-recaptcha-response').val(),
                post_status: $('#post_status').val(),
                post_type: $('#post_type').val(),
                // send the nonce along with the request
                nextNonce: url_ajax.nextNonce
            },
            beforeSend: function () {
                $('#btnSubmit').attr('data-loading-text', '<i class="fa fa-spinner fa-spin"></i>');
                $('#btnSubmit').button('loading');
            },
            success: function (response) {
                console.log(response);
                $('#divForm').html('');
                $('#divInfo').show();
            },
            error: function () {
                alert('Co van de khi xu ly. Vui long thu lai sau!');
            }
        });
    }
}