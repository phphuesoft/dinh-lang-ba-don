<?php
/**
 * @package HUESOFT
 * @Setup HUESOFT
 */
if ( !function_exists('Huesoft_theme_setup') ) {
    function Huesoft_theme_setup() {

        /* Thiet lap textdomain */
        $language_folder = THEME_URL . '/languages';
        load_theme_textdomain( 'Huesoft', $language_folder );

        /* Tu dong them link RSS len <head> **/
        add_theme_support( 'automatic-feed-links' );

        /* Them post thumbnail */
        add_theme_support( 'post-thumbnails' );

        /* Post Format */
        /*
        add_theme_support( 'post-formats', array(
            'image',
            'video',
            'gallery',
            'quote',
            'link'
        ) );
        */

        /* Them title-tag */
        add_theme_support( 'title-tag' );

        /* Them custom background */
        /*
        $default_background = array(
            'default-color' => '#e8e8e8'
        );
        add_theme_support( 'custom-background', $default_background );
        */

        /* Them menu */
        register_nav_menus( array(
            'main_menu' => __( 'Main menu', 'Huesoft' ),
            'menu_bottom' => __('Menu bottom', 'Huesoft'),
            'sitemap' => __('Sitemap', 'Huesoft'),
            'menu_policy' => __('Menu policy', 'Huesoft')
        ) );

    }
    add_action( 'init', 'Huesoft_theme_setup' );
}
?>