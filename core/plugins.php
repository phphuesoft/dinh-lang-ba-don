<?php
    function Huesoft_plugin_activation() {

        $plugins = array(
            // Plugin Google Map
            array(
                'name'      => 'Google Map Embed',
                'slug'      => 'gmap-embed', //Tên slug của plugin trên URL
                'required'  => false,
            ),

            // Plugin Yoast SEO
            array(
                'name'      => 'Yoast SEO',
                'slug'      => 'wordpress-seo', //Tên slug của plugin trên URL
                'required'  => false,
            ),

            // Plugin Phân Trang
            array(
                'name'      => 'WP-PageNavi',
                'slug'      => 'wp-pagenavi', //Tên slug của plugin trên URL
                'required'  => false,
            ),

            // Plugin Tạo Field
            array(
                'name'      => 'Advanced Custom Fields',
                'slug'      => 'advanced-custom-fields', //Tên slug của plugin trên URL
                'required'  => false,
            ),

            // Plugin CPT UI
            array(
                'name'      => 'Custom Post Type UI',
                'slug'      => 'custom-post-type-ui', //Tên slug của plugin trên URL
                'required'  => false,
            ),

            // Plugin Gallery
            // array(
            //     'name'      => 'Post Gallery',
            //     'slug'      => 'simple-post-gallery', //Tên slug của plugin trên URL
            //     'required'  => false,
            // ),

            // Plugin Form
            array(
                'name'      => 'Ninja Forms',
                'slug'      => 'ninja-forms', //Tên slug của plugin trên URL
                'required'  => false,
            ),

            // Plugin Show Logo
            // array(
            //     'name'      => 'Logo Showcase',
            //     'slug'      => 'logo-showcase', //Tên slug của plugin trên URL
            //     'required'  => false,
            // ),

            // Plugin Backup and Restore
            array(
                'name'      => 'Duplicator',
                'slug'      => 'duplicator', //Tên slug của plugin trên URL
                'required'  => false,
            ),

            array(
                'name'      => 'Redux Framework',
                'slug'      => 'redux-framework', //Tên slug của plugin trên URL hoặc tên thư mục
                'required'  => false,
            ),

            array(
                'name'      => 'TinyMCE Advanced',
                'slug'      => 'tinymce-advanced', //Tên slug của plugin trên URL hoặc tên thư mục
                'required'  => false,
            ),

        ); // end $plugins

        $config = array(
            'menu'         => 'wp-pagenavi, wordpress-seo, duplicator, logo-showcase, ninja-forms, simple-post-gallery, custom-post-type-ui, advanced-custom-fields, gmap-embed, tinymce-advanced', // Menu slug.
            'has_notices'  => false,                    // Có hiển thị thông báo hay không
            'dismissable'  => false,                    // Nếu đặt false thì người dùng không thể hủy thông báo cho đến khi cài hết plugin.
            'is_automatic' => true,                   // Nếu là false thì plugin sẽ không tự động kích hoạt khi cài xong.
        ); // end $config
        tgmpa( $plugins, $config );
    }
    add_action( 'tgmpa_register', 'Huesoft_plugin_activation' );
?>
