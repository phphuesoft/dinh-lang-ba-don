<?php
/**
 * @package HUESOFT
 */
/********************************************************************
 * Register Widget
 *********************************************************************/
add_action('widgets_init', 'huesoftWidgetsInit');
function huesoftWidgetsInit()
{
    if(function_exists('register_sidebar')){
        register_sidebar(
            array(
                'name'          => esc_html__('Header', 'Huesoft'),
                'id'            => 'header',
                'description'   => esc_html__('Hiển thị đầu trang', 'Huesoft'),
                'before_widget' => '<section id="%1$s" class="%2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>'
            )
        );
    }
    if(function_exists('register_sidebar')){
        register_sidebar(
            array(
                'name'          => esc_html__('Slider', 'Huesoft'),
                'id'            => 'hs-slider',
                'description'   => esc_html__('Hiển thị slider', 'Huesoft'),
                'before_widget' => '<section id="%1$s" class="%2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>'
            )
        );
    }

    register_sidebar(
        array(
            'name'          => esc_html__('Home', 'Huesoft'),
            'id'            => 'home',
            'description'   => esc_html__('Hiển thị nội dung trang chủ', 'Huesoft'),
            'before_widget' => '<section id="%1$s" class="%2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>'
        )
    );
    /**
     * Hiển thị tiện ích ở chân trang
     */
    register_sidebar(
        array(
            'name'          => esc_html__('Footer', 'Huesoft'),
            'id'            => 'footer',
            'description'   => esc_html__('Hiển thị chân trang', 'Huesoft'),
            'before_widget' => '<section id="%1$s" class="%2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_widget'  => '</h2>'
        )
    );
    /**
     * Tạo ra các widget
     */
    //register_widget("HsWidgetNews");
}



?>