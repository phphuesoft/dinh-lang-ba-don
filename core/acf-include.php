<?php

// 1. customize ACF path
add_filter( 'acf/settings/path', 'my_acf_settings_path' );
function my_acf_settings_path( $path ) {
	$path = get_stylesheet_directory() . '/plugin/advanced-custom-fields-pro/';

	return $path;
}

// 2. customize ACF dir
add_filter( 'acf/settings/dir', 'my_acf_settings_dir' );
function my_acf_settings_dir( $dir ) {
	$dir = get_stylesheet_directory_uri() . '/plugin/advanced-custom-fields-pro/';

	return $dir;
}

// 3. Hide ACF field group menu item
//add_filter('acf/settings/show_admin', '__return_false');

// 4. Include ACF
include_once get_template_directory() . '/plugin/advanced-custom-fields-pro/acf.php';

/**
 * Hide Advanced Custom Fields update notification
 */
//add_filter( 'site_transient_update_plugins', 'my_remove_update_nag' );
//function my_remove_update_nag( $value ) {
//	if ( isset( $value->response['advanced-custom-fields-pro/acf.php'] ) ) {
//		unset( $value->response['advanced-custom-fields-pro/acf.php'] );
//	}
//
//	return $value;
//}