<?php
/**
 * @package HUESOFT
 * @subpackage DAYSTAR
 * @Note Bổ sung các hàm cần thiết
 */
/*Lệnh bật tắt thanh admin ở trang giao diện*/
add_filter('show_admin_bar', '__return_false');
/*remove một số kênh không cần thiết*/
function wporg_remove_options_page()
{
    #remove_menu_page('edit-comments.php');
    #remove_submenu_page('plugins.php','plugin-editor.php');
    #remove_menu_page('tools.php');
}
add_action('admin_menu', 'wporg_remove_options_page', 99);
/**
@ Hàm hiển thị ảnh thumbnail của post.
@ Ảnh thumbnail sẽ không được hiển thị trong trang single
@ Nhưng sẽ hiển thị trong single nếu post đó có format là Image
@ Huesoft_thumbnail( $size )
 **/
if ( ! function_exists( 'Huesoft_thumbnail' ) ) {
    function Huesoft_thumbnail( $size ) {
        // Chỉ hiển thị thumbnail với post không có mật khẩu
        if ( !is_single() &&  has_post_thumbnail()  && !post_password_required() || has_post_format( 'image' )) : ?>
            <?php the_post_thumbnail( $size ); ?><?php
        endif;
    }
}
/**
@ Hàm hiển thị thông tin của post (Post Meta)
@ Huesoft_entry_meta()
 **/
if( ! function_exists( 'Huesoft_entry_meta' ) ) {
    function Huesoft_entry_meta() {
        if ( !is_page() ) :
            echo '<div class="entry-meta">';

            // Hiển thị tên tác giả
            printf( __('<span class="author">Posted by %1$s</span>', 'Huesoft'),
                get_the_author() );

            // Hiển thị tên category
            printf( __('<span class="date-published"> at %1$s</span>', 'Huesoft'),
                get_the_date() );

            // Hiển thị ngày tháng đăng bài
            printf( __('<span class="category"> in %1$s</span>', 'Huesoft'),
                get_the_category_list( ', ' ) );

            // Hiển thị số đếm lượt bình luận
            if ( comments_open() ) :
                echo ' <span class="meta-reply">';
                comments_popup_link(
                    __('Leave a comment', 'Huesoft'),
                    __('One comment', 'Huesoft'),
                    __('% comments', 'Huesoft'),
                    __('Read all comments', 'Huesoft')
                );
                echo '</span>';
            endif;
            echo '</div>';
        endif;
    }
}
/*
* Thêm chữ Read More vào excerpt
*/
function Huesoft_readmore() {
    return '...<p><a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Xem thêm', 'Huesoft') . '</a></p>';
}
add_filter( 'excerpt_more', 'Huesoft_readmore' );

/**
@ Hàm hiển thị nội dung của post type
@ Hàm này sẽ hiển thị đoạn rút gọn của post ngoài trang chủ (the_excerpt)
@ Nhưng nó sẽ hiển thị toàn bộ nội dung của post ở trang single (the_content)
@ Huesoft_entry_content()
 **/
if ( ! function_exists( 'Huesoft_entry_content' ) ) {
    function Huesoft_entry_content() {
        if ( !is_single() ) :
            the_excerpt();
        else :
            the_content();
        endif;
    }
}
/**
 * Huesoft custom field
 */
function Huesoft_get_field($selector, $post_id = false)
{
    if (function_exists('get_field')) {
        return get_field($selector, $post_id);
    } else {
        return false;
    }
}
function Huesoft_the_field($selector)
{
    if (function_exists('the_field')) {
        the_field($selector);
    }
}
/**
 * function favicon.ico in admin wordpress
 */
function add_favicon()
{
    $favicon_url = get_stylesheet_directory_uri() . '/favicon.ico';
    echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
}
add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');
/*get_image*/
function Huesoft_get_image_base64( $image_path, $is_full_path = false ) {
    if ( strlen( $image_path ) <= 0 ) {
        return '';
    }
    $path = $is_full_path ? $image_path : get_template_directory() . $image_path;
    $type = pathinfo( $path, PATHINFO_EXTENSION );
    $data = file_get_contents( $path );

    return 'data:image/' . $type . ';base64,' . base64_encode( $data );

}

function Huesoft_the_image_base64( $image_path, $is_full_path = false ) {
    if ( strlen( $image_path ) <= 0 ) {
        echo '';
    } else {
        $path = $is_full_path ? $image_path : get_template_directory() . $image_path;
        $type = pathinfo( $path, PATHINFO_EXTENSION );
        $data = file_get_contents( $path );
        echo 'data:image/' . $type . ';base64,' . base64_encode( $data );
    }
}
/*Huesoft related post*/
function Huesoft_related_post($text_other = 'Tin tức liên quan')
{
    global $post;
    $arr_cat = wp_get_post_categories($post->ID);
    if ($arr_cat[1] == ID_GALLERY){
        $text_other = "Album khác";
    }else if ($arr_cat[1] == ID_VIDEO){
        $text_other = "Video khác";
    }
    $args = array(
        'post_type'      => 'post',
        'cat'            => count($arr_cat) > 1 ? $arr_cat[1] : $arr_cat[0],
        'posts_per_page' => 6,
        'orderby'        => 'post_date',
        'order'          => 'DESC',
        'post__not_in'   => array($post->ID)
    );

    $query_repost = new WP_Query($args);
    ?>
    <?php
    if ($query_repost->have_posts()) {
        ?>
        <h2 class="hs-other-title text-center text-uppercase"><?php _e($text_other, 'Huesoft'); ?></h2>
        <div class="row hs-other-list">
                <?php
                while ($query_repost->have_posts()) {
                    $query_repost->the_post();
                    $video = Huesoft_get_field('hs_iframe_video');
                    ?>
                    <?php if ($arr_cat[1] == ID_GALLERY || $arr_cat[1] == ID_VIDEO){ ?>
                        <div class="col-md-4 col-sm-6 hs-media-list">
                            <div class="library-item">
                                <div class="box-image hs-item-thumbnail">
                                    <a href="<?php the_permalink() ?>"><img src="<?php has_post_thumbnail() ? the_post_thumbnail_url() : Huesoft_the_image_base64('/lib/images/noimage.gif');?>" class="img-fluid">
                                        <i class="fa <?php echo !empty($video) ? 'fa-play' : 'fa-picture-o'?>"></i></a>
                                </div>
                                <div class="box-content hs-item-content">
                                    <h5 class="hs-title-link text-center"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
                                </div>
                            </div>
                        </div>
                    <?php }else{ ?>
                        <div class="col-sm-6 col-md-4 card hs-box-item hs-box-scale" style="background-color: transparent;">
                            <div class="hs-item-image hs-item-thumbnail">
                                <a href="<?php the_permalink()?>">
                                    <img src="<?php the_post_thumbnail_url();?>" alt="<?php the_title()?>">
                                </a>
                            </div>
                            <div class="card-body hs-item-content">
                                <h5 class="hs-title-link"><a href="<?php the_permalink()?>"><?php the_title()?></a></h5>
                                <div class="hs-item-summary">
                                    <?php echo wp_trim_words(get_the_content(),30,'...');?>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                }
                ?>
        </div>
        <?php
    }
    ?>

    <?php
}
/*Huesoft Ajax Hỏi Đáp*/
add_action('wp_enqueue_scripts', 'question_submit_scripts');
add_action('wp_ajax_ajax-questionSubmit', 'ajax_questionSubmit_func');
add_action('wp_ajax_nopriv_ajax-questionSubmit', 'ajax_questionSubmit_func');

function question_submit_scripts()
{
    wp_enqueue_script('question_submit', get_template_directory_uri() . '/lib/js/question_submit.js', array('jquery'));
    wp_localize_script('question_submit', 'url_ajax', array(
            'ajaxurl'   => admin_url('admin-ajax.php'),
            'nextNonce' => wp_create_nonce('ajax-next-nonce')
        )
    );
}

function ajax_questionSubmit_func()
{
// check nonce
    $nonce = $_POST['nextNonce'];
    if (!wp_verify_nonce($nonce, 'ajax-next-nonce')) {
        die('Busted!');
    }

//google recaptcha
    if (isset($_POST['greCaptchaResponse']) && $_POST['greCaptchaResponse']) {
        $secret = GOOGLE_RECAPCHA_SECRET_KEY;
        $ip = $_SERVER['REMOTE_ADDR'];

        $captcha = $_POST['greCaptchaResponse'];
        $rsp = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $ip);
        $arr = json_decode($rsp, true);
        if ($arr['success']) {

//insert data
            $txtNameQA = sanitize_text_field($_POST["txtNameQA"]);
            $txtEmailQA = sanitize_email($_POST["txtEmailQA"]);
            $txtAnswerQA = sanitize_text_field($_POST["txtAnswerQA"]);
            $postStatus = sanitize_text_field($_POST["post_status"]);
            $postType = sanitize_text_field($_POST["post_type"]);

            $post = array(
                'post_title'  => $txtAnswerQA,
                'post_status' => $postStatus,
                'post_type'   => $postType,
                'meta_input'  => array(
                    'hs_question_fullname' => $txtNameQA,
                    'hs_question_email'    => $txtEmailQA,
                ),
            );
            wp_insert_post($post);
        } else {
            echo 'error';
        }
    }
// generate the response
    $response = json_encode($_POST);
// response output
    header("Content-Type: application/json");
    echo $response;
    wp_die();
}

add_action('wp_ajax_modal', 'huesoftAjaxModal');
add_action('wp_ajax_nopriv_modal', 'huesoftAjaxModal');
function huesoftAjaxModal(){
    ob_start();
    $post_id = isset($_GET['post_id']) ? $_GET['post_id'] : '';
    ?>
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-modal-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div id="showYoutube">
                        <div class="embed-responsive embed-responsive-16by9 hs-des-width">
                            <?php echo Huesoft_get_field('hs_iframe_video',$post_id);?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $result = ob_get_clean();
    wp_send_json_success($result);
    die();
}
?>