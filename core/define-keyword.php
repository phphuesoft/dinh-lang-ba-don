<?php
/**
 * @package HUESOFT
 * @description Define Keyword by Huesoft
 */
/* Khai bao reCaptcha */
define('GOOGLE_RECAPCHA_SITE_KEY', '6LcfXyIUAAAAAANBg7zO-TygwL_eA_9FB3x53EkW');
define('GOOGLE_RECAPCHA_SECRET_KEY', '6LcfXyIUAAAAAAf05rFCLqwKg1TAbFHRZb4HaFCt');
/*Positive*/
define('POST_TYPE_QUESTION','post_question');
define('PARENT_POSITIVE','catparent');
define('MEDIA_POSITIVE','media');
define('INTRO_POSITIVE','intro');
/*Define ID,SLUG follow Language*/
if(!function_exists(get_locale())){
    switch (get_locale()){
        case 'vi':
            /*define('ID_PAGE_ABOUT', 315);
            define('ID_CAT_NGHILE', 83);
            define('ID_CAT_HOATDONG', 87);
            define('ID_CAT_BANGVANG', 93);
            define('ID_CAT_KHUYENHOC', 97);
            define('ID_CAT_TAITRO', 99);*/
            define('ID_PAGE_ABOUT', 290);
            define('ID_CAT_NGHILE', 84);
            define('ID_CAT_HOATDONG', 88);
            define('ID_CAT_BANGVANG', 99);
            define('ID_CAT_KHUYENHOC', 116);
            define('ID_CAT_TAITRO', 112);
            define('ID_PAGE_INTRO',34);
            define('ID_CAT_MEDIA', 38);
            define('ID_PAGE_CONTACT', 15);
            define('ID_CAT_INTRO', 2);
            define('LANG','vi');
            define('ID_NEWS',13);
            define('ID_GALLERY',42);
            define('ID_VIDEO',44);
            define('CODE_CONTACT','[ninja_form id=1]');
            break;
        case 'en_GB':
            /*define('ID_PAGE_ABOUT', 318);
            define('ID_CAT_NGHILE', 85);
            define('ID_CAT_HOATDONG', 89);
            define('ID_CAT_BANGVANG', 999);
            define('ID_CAT_KHUYENHOC', 999);
            define('ID_CAT_TAITRO', 99);*/
            define('ID_PAGE_ABOUT', 297);
            define('ID_CAT_NGHILE', 86);
            define('ID_CAT_HOATDONG', 91);
            define('ID_CAT_BANGVANG', 99);
            define('ID_CAT_KHUYENHOC', 118);
            define('ID_CAT_TAITRO', 114);
            define('ID_PAGE_INTRO',180);
            define('ID_CAT_MEDIA', 62);
            define('ID_PAGE_CONTACT', 17);
            define('ID_CAT_INTRO', 46);
            define('LANG','en');
            define('ID_NEWS',56);
            define('ID_GALLERY',64);
            define('ID_VIDEO',66);
            define('CODE_CONTACT','[ninja_form id=2]');
            break;
    }
}
?>