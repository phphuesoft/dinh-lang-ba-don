<?php
/**
 * Huesoft Enqueue
 *
 * @package HUESOFT
 */

if ( !function_exists('Huesoft_enqueue') ){
    function Huesoft_enqueue(){
        $the_theme = wp_get_theme();
        wp_enqueue_style( 'Huesoft-bootstrap', get_stylesheet_directory_uri() . '/lib/css/bootstrap.min.css', array(), $the_theme->get( 'Version' ), false );
        wp_enqueue_style( 'Huesoft-font-awesome', get_stylesheet_directory_uri() . '/lib/css/font-awesome.min.css', array(), $the_theme->get( 'Version' ), false );
        wp_enqueue_style( 'Huesoft-fancybox', get_stylesheet_directory_uri() . '/lib/fancybox/jquery.fancybox.css', array(), $the_theme->get( 'Version' ), false );
        wp_enqueue_style( 'Huesoft-fancybox-button', get_stylesheet_directory_uri() . '/lib/fancybox/helpers/jquery.fancybox-buttons.css', array(), $the_theme->get( 'Version' ), false );
        wp_enqueue_style( 'Huesoft-owl-carousel', get_stylesheet_directory_uri() . '/lib/css/owl.carousel.min.css', array(), $the_theme->get( 'Version' ), false );
        wp_enqueue_style( 'Huesoft-owl-carousel-default', get_stylesheet_directory_uri() . '/lib/css/owl.theme.default.min.css', array(), $the_theme->get( 'Version' ), false );
        wp_enqueue_style( 'Huesoft-style', get_stylesheet_directory_uri() . '/style.css', array(), $the_theme->get( 'Version' ), false );
        wp_enqueue_style( 'Huesoft-style-tnquang', get_stylesheet_directory_uri() . '/style-tnquang.css', array(), $the_theme->get( 'Version' ), false );
        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'Huesoft-bootstrap-jquery', get_template_directory_uri() . '/lib/js/bootstrap.min.js', array(), $the_theme->get( 'Version' ), true );
        wp_enqueue_script( 'Huesoft-popper', get_template_directory_uri() . '/lib/js/popper.min.js', array(), true );
        wp_enqueue_script( 'Huesoft-fancybox-mousewheel-jquery', get_template_directory_uri() . '/lib/fancybox/jquery.mousewheel.pack.js', array(), $the_theme->get( 'Version' ), true );
        wp_enqueue_script( 'Huesoft-fancybox-jquery', get_template_directory_uri() . '/lib/fancybox/jquery.fancybox.pack.js', array(), $the_theme->get( 'Version' ), true );
        wp_enqueue_script( 'Huesoft-fancybox-buttons-query', get_template_directory_uri() . '/lib/fancybox/helpers/jquery.fancybox-buttons.js', array(), $the_theme->get( 'Version' ), true );
        wp_enqueue_script( 'Huesoft-owl-carousel-js', get_template_directory_uri() . '/lib/js/owl.carousel.min.js', array(), $the_theme->get( 'Version' ), true );
    }
}
add_action('wp_enqueue_scripts','Huesoft_enqueue');