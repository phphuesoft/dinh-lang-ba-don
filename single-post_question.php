<?php get_header(); ?>
<section class="container hs-section-top">
    <div class="hs-article-detail">
            <h1 class="hs-title hs-title-normal text-uppercase text-center"><span class="title-span"><?php _e('Hỏi đáp','Huesoft');?></span></h1>
            <div class="hs-info-question">
                <h2 class="hs-title-question"><?php _e('Câu trả lời','Huesoft');?></h2>
                <div class="hs-content-question">
                <?php if (have_posts()) : the_post(); $id_single = get_the_ID();?>
                    <h3 class="hs-question"><i class="fa fa-question"></i> <?php _e('Hỏi','Huesoft');?></h3>
                    <h4 class="hs-article-title"><?php the_title(); ?></h4>
                    <p><span><b><?php Huesoft_the_field('hs_question_fullname');?></b></span> - <span><?php _e('Email','Huesoft')?>: <a href="mailto:<?php Huesoft_the_field('hs_question_email')?>"><?php Huesoft_the_field('hs_question_email')?></a></span> (<?php the_date('d/m/Y')?>)</p>
                    <h3 class="hs-question"><i class="fa fa-reply"></i> <?php _e('Trả lời','Huesoft');?></h3>
                    <div class="hs-article-content">
                        <?php the_content(); ?>
                    </div>
                <?php endif; ?>
                </div>
            </div>
            <div class="hs-other">
                <?php
                $other_query = new WP_Query(
                    array(
                        'post_type' => POST_TYPE_QUESTION,
                        'posts_per_page' => 10,
                        'orderby' => 'post_date',
                        'order'  => 'DESC',
                        'post__not_in' => array($id_single)
                    )
                );
                if($other_query->have_posts()):
                    ?>
                    <h2 class="hs-title-question"><?php _e('Câu hỏi khác','Huesoft');?></h2>
                        <?php
                        while($other_query->have_posts()):
                            $other_query->the_post();
                                ?>
                                <div class="hs-list-qa">
                                    <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                                    <div class="hs-qa-content">
                                        <i class="fa fa-arrow-right"></i> <?php echo wp_trim_words(get_the_content(),30,'...');?>
                                    </div>
                                </div>
                            <?php
                        endwhile;
                endif;
                ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>