<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title"
          content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri()?>/favicon.ico">
	<link rel="profile" href="http://gmgp.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php wp_head(); ?>
    <?php global $options; ?>
</head>
<body <?php body_class(); ?> >
<header class="hs-header d-none d-lg-block">
    <div class="container">
        <div class="row">
            <!--<div class="col-12 col-md-6 order-1 order-lg-2">
                <div class="hs-logo text-center">
                    <?php /*if ( has_custom_logo() ) : the_custom_logo(); else : */?>
                        <a class="navbar-brand" rel="home" href="<?php /*echo esc_url( home_url( '/' ) ); */?>"
                           title="<?php /*echo esc_attr( get_bloginfo( 'name', 'display' ) ); */?>">
                            <img src="<?php /*Huesoft_the_image_base64( '/lib/images/logo.png' ) */?>"
                                 alt="<?php /*echo get_bloginfo( 'name', 'display' ) */?>"/>
                        </a>
                    <?php /*endif; */?>
                </div>
            </div>-->
            <div class="col-sm-6 text-left">
                <p class="hs-hotline"><a href="tel:<?php echo $options['hotline'];?>"><i class="fa fa-phone"></i> <span class="d-none d-md-inline-block"><?php _e('Điện thoại','Huesoft'); ?> <?php echo $options['hotline'];?></span></a></p>
            </div>
            <div class="col-sm-6 text-right">
                <div class="hs-lang">
                    <?php
                    if (function_exists('pll_the_languages')) {
                        ?>
                        <ul class="list-unstyled">
                            <?php
                            pll_the_languages(array(
                                'show_names' => 0,
                                'show_flags' => 0,
                                'hide_if_empty' => 0,
                                'hide_current' => 0
                            ));
                            ?>
                        </ul>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</header>
<nav class="navbar navbar-expand-lg hs-navbar">
    <div class="container">
        <div class="hs-lang d-inline-block d-lg-none">
            <?php
            if (function_exists('pll_the_languages')) {
                ?>
                <ul class="list-unstyled">
                    <li class="d-lg-none d-inline-block"><a class="hs-hotline-m" href="tel:<?php echo $options['hotline'];?>"><i class="fa fa-phone"></i> <span><?php _e('Điện thoại','Huesoft'); ?> <?php echo $options['hotline'];?></span></a></li>
                    <?php
                    pll_the_languages(array(
                        'show_names' => 0,
                        'show_flags' => 0,
                        'hide_if_empty' => 0,
                        'hide_current' => 0
                    ));
                    ?>
                </ul>
                <?php
            }
            ?>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-navicon"></span>
        </button>
        <?php wp_nav_menu(
            array(
                'theme_location'  => 'main_menu',
                'container_class' => 'collapse navbar-collapse',
                'container_id'    => 'navbarNavDropdown',
                'menu_class'      => 'navbar-nav mx-auto',
                'fallback_cb'     => '',
                'menu_id'         => 'main-menu',
                'walker'          => new huesoft_WP_Bootstrap_Navwalker(),
            )
        ); ?>
    </div>
</nav>
<section class="hs-slider">
    <?php echo do_shortcode('[smartslider3 slider=2]');?>
</section>
