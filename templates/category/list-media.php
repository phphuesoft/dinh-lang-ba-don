<?php $video = Huesoft_get_field('hs_iframe_video'); ?>
<div class="col-md-4 col-sm-6 hs-media-list">
    <div class="library-item">
        <div class="box-image hs-item-thumbnail">
            <a href="<?php the_permalink() ?>"><img src="<?php has_post_thumbnail() ? the_post_thumbnail_url() : Huesoft_the_image_base64('/lib/images/noimage.gif');?>" class="img-fluid">
            <i class="fa <?php echo !empty($video) ? 'fa-play' : 'fa-picture-o'?>"></i></a>
        </div>
        <div class="box-content hs-item-content">
            <h5 class="hs-title-link text-center"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
        </div>
    </div>
</div>