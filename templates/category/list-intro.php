<div class="col-md-4 card hs-box-item hs-box-scale" style="background-color: transparent;">
    <div class="hs-item-image hs-item-thumbnail">
        <a href="<?php the_permalink()?>"><img src="<?php has_post_thumbnail() ? the_post_thumbnail_url() : Huesoft_the_image_base64('/lib/images/noimage.gif');?>" alt="<?php the_title()?>"></a>
    </div>
    <div class="card-body hs-item-content">
        <h5 class="hs-title-link"><a href="<?php the_permalink()?>"><?php the_title()?></a></h5>
        <div class="hs-item-summary">
            <?php the_content();?>
        </div>
    </div>
    <div class="card-footer hs-item-link">
        <a class="hs-readmore hs-link-scale text-uppercase" href="<?php the_permalink()?>" title="<?php the_title()?>"><?php _e('Xem thêm','Huesoft');?></a>
    </div>
</div>