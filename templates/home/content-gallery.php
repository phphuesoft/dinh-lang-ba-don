<section class="hs-section-bottom">
    <div class="container">
        <div class="hs-box">
            <h4 class="hs-title hs-title-normal text-uppercase text-center">
                <span class="title-span"><a href="<?php echo esc_url(get_category_link(ID_GALLERY));?>"><?php echo get_cat_name(ID_GALLERY);?></a></span>
            </h4>
            <div class="hs-des text-center hs-des-width">
                <?php echo category_description(ID_GALLERY);?>
            </div>
        </div>
        <div class="hs-box-list">
            <div class="row">
                <?php
                $loop_gallery = new WP_Query(array(
                    'post_type'         => 'post',
                    'orderby'           => 'post_date',
                    'order'             => 'Desc',
                    'posts_per_page'    => 1,
                    'cat'      => ID_GALLERY
                ));
                if ($loop_gallery->have_posts()):
                    while ($loop_gallery->have_posts()): $loop_gallery->the_post();
                    $hs_album = Huesoft_get_field('hs_gallery',get_the_ID());
                    if($hs_album){
                    $arr = [];
                    $i = 0;
                    foreach ($hs_album as $hs_gallery): $i++;
                    if ($i == 1){
                        $arr[] = [$hs_gallery['url'],$hs_gallery['caption']];
                    }elseif ($i == 2){
                        $arr[] = [$hs_gallery['url'],$hs_gallery['caption']];
                    }elseif ($i == 3){
                        $arr[] = [$hs_gallery['url'],$hs_gallery['caption']];
                    }elseif ($i == 4){
                        $arr[] = [$hs_gallery['url'],$hs_gallery['caption']];
                    }elseif($i == 5){
                        $arr[] = [$hs_gallery['url'],$hs_gallery['caption']];
                    }else{
                    ?>
                    <a href="<?php echo $hs_gallery['url'];?>" class="fancybox-home d-none" data-fancybox-group="button" title="<?php echo $hs_gallery['caption'];?>">
                        <img src="<?php echo $hs_gallery['url'];?>" alt="<?php echo $hs_gallery['caption'];?>" width="100%"/>
                        <i class="fa fa-search"></i>
                    </a>
                    <?php
                    }
                    endforeach;
                    ?>
                    <div class="col-md-8 col-lg-9">
                        <div class="row">
                            <div class="col-6 col-md-7 col-lg-8 hs-pad-right hs-box-gallery hs-item-thumbnail hs-thumbnail-normal">
                                <a href="<?php echo $arr[0][0];?>" class="fancybox-home" data-fancybox-group="button" title="<?php echo $arr[0][1];?>">
                                    <img src="<?php echo $arr[0][0];?>" alt="<?php echo $arr[0][1];?>" width="100%"/>
                                    <i class="fa fa-search"></i>
                                </a>
                            </div>
                            <div class="col-6 col-md-5 col-lg-4 hs-pad-left hs-box-gallery hs-item-thumbnail hs-thumbnail-small">
                                <a href="<?php echo $arr[1][0];?>" class="fancybox-home" data-fancybox-group="button" title="<?php echo $arr[1][1];?>">
                                    <img src="<?php echo $arr[1][0];?>" alt="<?php echo $arr[1][1];?>" width="100%"/>
                                    <i class="fa fa-search"></i>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 col-md-5 col-lg-4 hs-pad-right hs-box-gallery hs-item-thumbnail hs-thumbnail-small">
                                <a href="<?php echo $arr[2][0];?>" class="fancybox-home" data-fancybox-group="button" title="<?php echo $arr[2][1];?>">
                                    <img src="<?php echo $arr[2][0];?>" alt="<?php echo $arr[2][1];?>" width="100%"/>
                                    <i class="fa fa-search"></i>
                                </a>
                            </div>
                            <div class="col-6 col-md-7 col-lg-8 hs-pad-left hs-box-gallery hs-item-thumbnail hs-thumbnail-normal">
                                <a href="<?php echo $arr[3][0];?>" class="fancybox-home" data-fancybox-group="button" title="<?php echo $arr[3][1];?>">
                                    <img src="<?php echo $arr[3][0];?>" alt="<?php echo $arr[3][1];?>" width="100%"/>
                                    <i class="fa fa-search"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3 hs-padd-left d-none d-md-inline-block">
                        <div class="hs-box-gallery hs-item-thumbnail hs-thumbnail-large">
                            <a href="<?php echo $arr[4][0];?>" class="fancybox-home" data-fancybox-group="button" title="<?php echo $arr[4][1];?>">
                                <img src="<?php echo $arr[4][0];?>" alt="<?php echo $arr[4][1];?>" width="100%"/>
                                <i class="fa fa-search"></i>
                            </a>
                        </div>
                    </div>
                <?php
                }
                    endwhile;
                endif;
                ?>
            </div>
        </div>
    </div>
</section>