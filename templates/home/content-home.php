<section class="hs-section-top">
    <div class="container">
        <div class="hs-box">
            <div class="hs-box-title">
                <h1 class="hs-title hs-title-home text-uppercase text-center"><span class="title-span"><?php _e('Giới thiệu','Huesoft') ?></span></h1>
            </div>
            <?php
            $loop_page = new WP_Query('page_id='.ID_PAGE_ABOUT);
            if ($loop_page->have_posts()):
                $loop_page->the_post();
                ?>
                <h1 class="hs-title-child text-uppercase text-center">
                    <?php the_title();?>
                </h1>
                <div class="hs-des text-center">
                    <?php the_content();?>
                </div>
            <?php
            endif;
            ?>
        </div>
        <div class="hs-box-list">
            <div class="row">
                <div class="card-group">
                <?php
                    $loop_intro = new WP_Query(array(
                            'post_type'         => 'post',
                            'orderby'           => 'post_date',
                            'order'             => 'Desc',
                            'posts_per_page'    => 3,
                            'cat'               => ID_CAT_NGHILE
                    ));
                if ($loop_intro->have_posts()):
                    while ($loop_intro->have_posts()): $loop_intro->the_post();
                        ?>
                        <div class="col-md-4 card hs-box-item hs-box-scale" style="background-color: transparent;">
                            <div class="hs-item-image hs-item-thumbnail">
                                <a href="<?php the_permalink()?>"><img src="<?php the_post_thumbnail_url();?>" alt="<?php the_title()?>"></a>
                            </div>
                            <div class="card-body hs-item-content">
                                <h5 class="hs-title-link text-uppercase"><a href="<?php the_permalink()?>"><?php the_title()?></a></h5>
                                <div class="hs-item-summary">
                                    <?php echo wp_trim_words(get_the_content(),30,'...');?>
                                </div>
                            </div>
                            <div class="card-footer hs-item-link">
                                <a class="hs-readmore hs-link-scale" href="<?php the_permalink()?>" title="<?php the_title()?>"><?php _e('Xem Thêm','Huesoft');?></a>
                            </div>
                        </div>
                    <?php
                    endwhile;
                endif;
                wp_reset_query();
                ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="hs-section-middle container-fluid">
    <div class="row">
        <div class="col-lg-6 hs-image-avatar hs-padd" style="padding-left: 0; padding-right: 0;">
            <?php $cat_img_hoatdong = Huesoft_get_field('category_image', "category_" .ID_CAT_HOATDONG); ?>
            <img src="<?php echo $cat_img_hoatdong['url'] ?>">
        </div>
        <div class="col-lg-6" style="padding-left: 0; padding-right: 0;">
            <div class="hs-box hs-positive">
                <h3 class="hs-title-action text-uppercase">
                    <?php echo get_cat_name(ID_CAT_HOATDONG);?>
                </h3>
                <div class="hs-des-action hs-item-content">
                    <?php echo category_description(ID_CAT_HOATDONG);?>
                </div>
                <div class="hs-item-link">
                    <a class="hs-readmore hs-link-action" href="<?php echo esc_url(get_category_link(ID_CAT_HOATDONG)); ?>" title="<?php the_title()?>"><?php _e('Xem Thêm','Huesoft');?></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="hs-section-middle-bottom container">
    <div class="hs-box">
        <div class="hs-box-title">
            <h1 class="hs-title hs-title-normal text-uppercase text-center"><span class="title-span"><span><?php echo get_cat_name(ID_CAT_BANGVANG); ?></span></span></h1>
        </div>
        <div class="hs-des text-center hs-des-width">
            <?php echo category_description(ID_CAT_BANGVANG); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5 card">
            <div class="hs-card-image hs-item-thumbnail">
                <?php $cat_img_taitro = Huesoft_get_field('category_image', "category_" .ID_CAT_TAITRO); ?>
                <a href="<?php echo esc_url(get_category_link(ID_CAT_TAITRO)); ?>"><img src="<?php echo $cat_img_taitro['url'] ?>" alt="<?php echo get_cat_name(ID_CAT_TAITRO);?>"></a>
            </div>
            <div class="card-body hs-item-content">
                <h5 class="hs-title-normal"><a href="<?php echo esc_url(get_category_link(ID_CAT_TAITRO)); ?>"><?php echo get_cat_name(ID_CAT_TAITRO);?></a></h5>
                <div class="hs-item-summary">
                    <?php echo category_description(ID_CAT_TAITRO);?>
                </div>
            </div>
            <div class="card-footer hs-item-link">
                <a class="hs-readmore hs-link-action" href="<?php echo esc_url(get_category_link(ID_CAT_TAITRO)); ?>" title="<?php echo get_cat_name(ID_CAT_TAITRO);?>"><?php _e('Xem Thêm','Huesoft');?></a>
            </div>
        </div>
        <div class="col-lg-7 card">
            <div class="hs-card-avatar hs-card-image hs-item-thumbnail">
                <?php $cat_img_khuyenhoc = Huesoft_get_field('category_image', "category_" .ID_CAT_KHUYENHOC); ?>
                <a href="<?php echo esc_url(get_category_link(ID_CAT_KHUYENHOC)); ?>"><img src="<?php echo $cat_img_khuyenhoc['url']; ?>" alt="<?php echo get_cat_name(ID_CAT_KHUYENHOC);?>"></a>
            </div>
            <div class="card-body hs-item-content">
                <h5 class="hs-title-normal"><a href="<?php echo esc_url(get_category_link(ID_CAT_KHUYENHOC)); ?>"><?php echo get_cat_name(ID_CAT_KHUYENHOC);?></a></h5>
                <div class="hs-item-summary">
                    <?php echo category_description(ID_CAT_KHUYENHOC);?>
                </div>
            </div>
            <div class="card-footer hs-item-link">
                <a class="hs-readmore hs-link-action" href="<?php echo esc_url(get_category_link(ID_CAT_KHUYENHOC)); ?>" title="<?php echo get_cat_name(ID_CAT_KHUYENHOC);?>"><?php _e('Xem Thêm','Huesoft');?></a>
            </div>
        </div>
    </div>
</section>
<?php get_template_part('templates/home/content','gallery');?>