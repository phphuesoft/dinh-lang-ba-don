<?php get_header(); ?>
<section class="container hs-section-top">
    <div class="hs-article-list">
        <h1 class="hs-title text-uppercase text-center"><?php _e('Tìm kiếm', 'Huesoft'); ?></h1>
        <div class="hs-search hs-box">
            <div class="row">
                <div class="mx-auto frmSearch">
                    <?php $unique_id = esc_attr(uniqid('search-form-')); ?>
                    <form role="search" method="get" class="form-inline" action="<?php echo esc_url(home_url('/')); ?>">
                        <input class="form-control" type="search" id="<?php echo $unique_id; ?>"
                               placeholder="<?php _e('Tìm kiếm', 'Huesoft'); ?>..."
                               aria-label="<?php _e('Tìm kiếm', 'Huesoft'); ?>" value="<?php echo get_search_query(); ?>"
                               name="s">
                        <button type="submit" class="hs-btn hs-btn-blue"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>
            <div class="row">
            <?php
            $_title = $_GET['s'] != '' ? $_GET['s'] : '';
            $args_search = array(
                'post_type' 	=> 'post',
                's'	=> $_title,
                'category__in' => array(ID_NEWS),
                'posts_per_page'	=> 9
            );
            $loop_search = new WP_Query($args_search);
            $post_counts = $loop_search->post_count;
            ?>
            <?php if ($loop_search->have_posts()): ?>
                <h4 class="col-12 notfound"><?php _e('Có','Huesoft')?> <?php echo $post_counts;?> <?php _e('kết quả tìm kiếm', 'Huesoft'); ?>: "<?php echo get_search_query(); ?>"</h4>
            <?php while ($loop_search->have_posts()): $loop_search->the_post(); ?>
            <?php get_template_part('templates/category/list');?>
            <?php endwhile; ?>
                <div class="col-12 hs-pagination"><?php wp_pagenavi(array( 'query' => $loop_search )); ?></div>
            <?php
            else:
            ?>
                <div class="text-center">
                    <h4 class="notfound"><?php _e('Không tìm thấy kết quả nào', 'Huesoft'); ?>:
                        "<?php echo get_search_query(); ?>"</h4>
                </div>
            <?php
            endif;
            wp_reset_postdata();
            ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>