<?php
/*
 * Template Name: Default
 * @package Huesoft
 */
?>
<?php get_header(); ?>
    <section class="container hs-section-top">
        <div class="hs-article-detail">
            <?php if ( have_posts() ) : the_post(); ?>
                <h1 class="hs-title hs-title-normal text-uppercase text-center"><span class="title-span"><?php the_title();?></span></h1>
                <div class="hs-article-content">
                    <?php the_content(); ?>
                </div>
            <?php endif; ?>
        </div>
    </section>
<?php get_footer(); ?>