<article id="post-<?php the_ID();?>" <?php post_class(); ?> >
	<div class="entry-thumbnail">
		<?php Huesoft_thumbnail('thumbnail'); ?>
	</div>
	<div class="entry-header">
		<?php Huesoft_entry_meta(); ?>
	</div>
	<div class="entry-content">
		<?php Huesoft_entry_content(); ?>
		<?php ( is_single() ? Huesoft_entry_tag() : '' ); ?>
	</div>
</article>