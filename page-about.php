<?php
/*
 * Template Name: About Us
 * @package Huesoft
 */
?>
<?php global $options;?>
<?php get_header(); ?>
    <section class="container hs-section-top hs-about">
        <div class="hs-article-detail">
            <?php if ( have_posts() ) : the_post(); ?>
                <h1 class="hs-title hs-title-normal text-uppercase text-center"><span class="title-span"><?php the_title();?></span></h1>
                <div class="row">
                    <div class="col-lg-6">
                        <h2 class="hs-title-contact"><?php _e('Đình Làng Ba Đồn Quảng Bình','Huesoft');?></h2>
                        <div class="hs-content-contact">
                            <p><i class="fa fa-info"></i><?php _e('Tên di tích','Huesoft');?>: <?php huesoft_the_field('hs_about_name')?></p>
                            <p><i class="fa fa-cog"></i><?php _e('Loại công trình','Huesoft');?>: <?php huesoft_the_field('hs_about_type')?></p>
                            <p><i class="fa fa-university"></i><?php _e('Loại di tích','Huesoft');?>: <?php huesoft_the_field('hs_about_ancient')?></a></p>
                            <p><i class="fa fa-map-marker"></i><?php _e('Địa chỉ di tích','Huesoft');?>: <?php huesoft_the_field('hs_about_address')?></p>
                            <?php the_content();?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15199.19515160586!2d106.4234897!3d17.7541175!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x481f523e0526aa30!2zxJDDrG5oIGzDoG5nIEJhIMSQ4buTbg!5e0!3m2!1svi!2s!4v1544963273517" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </section>
<?php get_footer(); ?>