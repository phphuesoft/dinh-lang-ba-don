<?php
	/**
	@ Khai bao hang gia tri
		@ THEME_URL = lay duong dan thu muc theme
		@ CORE = lay duong dan cua thu muc /core
	**/
	define( 'THEME_URL', get_stylesheet_directory() );
	define ( 'CORE', THEME_URL . "/core" );
	/**
	@ Nhung file /core/init.php
	**/
	require_once(CORE . "/init.php" );
	require_once(CORE . "/define-keyword.php" );
	require_once(CORE . "/widgets.php");
	require_once(CORE . "/bootstrap-wp-navwalker.php");
	require_once(CORE . "/setup.php");
	require_once(CORE . "/enqueue.php");
	require_once(CORE . "/custom-setup.php");

?>
