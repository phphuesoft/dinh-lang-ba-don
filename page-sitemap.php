<?php
/**
 * Template Name: Sitemap
 * @package Huesoft
 */
?>
<?php get_header(); ?>
<section class="container hs-section-top">
    <div class="hs-article-detail">
        <?php
            if(have_posts()):
                the_post();
                ?>
                <h1 class="hs-title hs-title-normal text-uppercase text-center">
                    <span class="title-span"><?php the_title();?></span>
                </h1>
            <?php
            endif;
        ?>
        <?php
        wp_nav_menu(
            array(
                'theme_location'  => 'sitemap',
                'container_class' => 'hs-sitemap',
                'menu_class'      => 'hs-navbar-sitemap hs-des-width',
            )
        );
        ?>
    </div>
</section>
<?php get_footer(); ?>