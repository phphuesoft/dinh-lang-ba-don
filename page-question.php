<?php
/**
 * Template Name: Answer
 *
 * Template for displaying a page send form question.
 *
 * @package Huesoft
 */
?>
<?php get_header(); ?>
<script src='https://www.google.com/recaptcha/api.js?hl=<?php echo LANG; ?>'></script>
<section class="container hs-section-top">
    <div class="hs-article-list">
            <h1 class="hs-title hs-title-normal text-uppercase text-center"><span class="title-span"><?php _e('Hỏi đáp', 'Huesoft'); ?></span></h1>
        <div class="row">
            <div class="col-lg-6 order-0 order-lg-1">
                <h2 class="hs-title-question"><?php _e('Trả lời câu hỏi', 'Huesoft');?></h2>
                <?php
                /*Danh sách câu hỏi*/
                $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                $args = array(
                    'post_type' => POST_TYPE_QUESTION,
                    'orderby'   => 'post_date',
                    'order'     => 'Desc',
                    'paged'     => $paged
                );
                $loop_args = new WP_Query($args);
                if ($loop_args->have_posts()):
                    $i = 0;
                    while ($loop_args->have_posts()):
                        $loop_args->the_post(); $i++;
                        ?>
                        <div class="hs-list-qa">
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <div class="hs-qa-content">
                                <i class="fa fa-arrow-right"></i> <?php echo wp_trim_words(get_the_content(), 30, '...'); ?>
                            </div>
                        </div>
                    <?php
                    endwhile;
                    ?>
                    <div class="hs-home-pagination">
                        <?php wp_pagenavi();?>
                    </div>
                <?php
                endif;
                wp_reset_query();
                ?>
            </div>
            <div class="col-lg-6 order-1 order-lg-0">
                <div class="hs-form">
                    <h2 class="hs-title-question"><?php _e('Đặt câu hỏi', 'Huesoft');?></h2>
                    <form action="" method="post" name="frmFilterQA" id="frmFilterQA">
                        <div id="divInfo" style="display: none;">
                            <?php _e('Cảm ơn bạn đã gửi câu hỏi! <br/>Chúng tôi đã nhận được thông tin của bạn và sẽ trả lời với bạn trong thời gian sớm nhất!', 'Huesoft'); ?>
                        </div>
                        <div id="divForm">
                            <div class="form-group">
                                <label><?php _e('Viết câu hỏi vào ô dưới đây', 'Huesoft'); ?> <font color="#ff0000">*</font></label>
                                <textarea name="txtAnswerQA" id="txtAnswerQA" class="form-control" rows="3"/></textarea>
                            </div>
                            <div class="form-group">
                                <label><?php _e('Họ và tên', 'Huesoft'); ?> <font color="#ff0000">*</font></label>
                                <input type="text" name="txtNameQA" id="txtNameQA" class="form-control form-control-sm"/>
                            </div>
                            <div class="form-group">
                                <label><?php _e('Email', 'Huesoft'); ?> <font color="#ff0000">*</font></label>
                                <input type="email" name="txtEmailQA" id="txtEmailQA" class="form-control form-control-sm"/>
                            </div>
                            <div class="form-group">
                                <label><?php _e('Mã bảo vệ', 'Huesoft'); ?> <font color="#ff0000">*</font></label>
                                <div class="g-recaptcha"
                                     data-sitekey="<?php echo GOOGLE_RECAPCHA_SITE_KEY; ?>"></div>
                            </div>
                            <div class="form-group">
                                <input type="hidden" id="post_status" name="post_status" value="<?php echo 'pending'; ?>"/>
                                <input type="hidden" name="post_type" id="post_type"
                                       value="<?php echo POST_TYPE_QUESTION; ?>"/>
                                <button type="button" class="btn btn-sm btn-blue" id="btnSubmit" name="btnSubmit"
                                        onclick="frmQA(); return false;"/>
                                <?php _e('Đặt câu hỏi', 'Huesoft'); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
