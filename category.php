<?php get_header(); ?>
<section class="container hs-section-top">
    <?php
        $term = get_queried_object();
        $cat_term_id = $term->term_id;

        $showCatParent = false;

        $positive = get_field('hs_positive', $term);
        if ($positive == PARENT_POSITIVE){
            $catMediaChildren = get_terms($term->taxonomy, array(
                'parent' => $term->term_id,
                'hide_empty' => false
            ));

            if ($catMediaChildren) {
                $showCatParent = true;
            }
        }

    ?>
    <div class="hs-article-list">
    <?php if ($showCatParent) { ?>
        <?php foreach ($catMediaChildren as $cat){ ?>
            <h2 class="hs-title-question"><?php echo $cat->name; ?></h2>
            <div class="row">
                <?php
                    $loop_query = new WP_Query( array('post_count' => 10, 'cat' => $cat->term_id) );
                    if ($loop_query->have_posts()) :
                        while($loop_query->have_posts()): $loop_query->the_post();
                            get_template_part('templates/category/list','parent');
                        endwhile;
                    endif;
                ?>
            </div>
        <?php } ?>
    <?php }else{ ?>

            <h1 class="hs-title hs-title-normal text-uppercase text-center"><span class="title-span"><?php echo get_cat_name($cat_term_id)?></span></h1>
        <?php
        $loop_cat = new WP_Query(
            array(
                'cat' => get_query_var('cat'),
                'post_type' => 'post',
                'orderby' => 'post_date',
                'order'		=> 'DESC',
                'posts_per_page' => 10
            )
        );
        $post_count = $loop_cat->post_count;
        if($post_count == 1):
            if ($loop_cat->have_posts()): $loop_cat->the_post();
            echo "<div id='detail'>";
            get_template_part('templates/category/list','content');
            echo "</div>";
            endif;
        else:
            ?>
        <div class="row">
            <?php if ( $loop_cat->have_posts() ) : while( $loop_cat->have_posts() ) : $loop_cat->the_post(); ?>
                <?php
                if($positive == MEDIA_POSITIVE)
                    get_template_part('templates/category/list','media');
                else if($positive == INTRO_POSITIVE)
                    get_template_part('templates/category/list','intro');
                else get_template_part('templates/category/list');
                ?>
                <?php endwhile ?>
                <?php wp_pagenavi(); ?>
                <?php else: ?>
                <div class="col-12 text-center">
                    <?php _e('Dữ liệu đang cập nhật. Vui lòng quay lại sau!','Huesoft');?>
                </div>
            <?php endif;?>
        </div>
        <?php endif; ?>
        </div>
    <?php }?>
</section>
<?php get_footer(); ?>